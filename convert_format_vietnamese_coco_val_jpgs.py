import json


def main():
    source = '/mlcv/Databases/Imagecaption/Dataset/Vietnamese-COCO/annotations/caption_cc_val.json'
    data = json.load(open(source, 'r', encoding='utf-8'))
    print(data)
    result = {}
    for image in data['images']:
        result[image['file_name']] = image['file_name']

    json.dump(result, open('/mlcv/Databases/Imagecaption/Dataset/Vietnamese-COCO/annotations/coco_valid_jpgs.json', 'w', encoding='utf-8'))


if __name__ == '__main__':
    main()
