# Use Caffe2 image as parent image
FROM caffe2/caffe2:snapshot-py2-cuda9.0-cudnn7-ubuntu16.04

RUN apt-get update
RUN apt-get install vim wget -y

RUN mv /usr/local/caffe2 /usr/local/caffe2_build
ENV Caffe2_DIR /usr/local/caffe2_build

ENV PYTHONPATH /usr/local/caffe2_build:${PYTHONPATH}
ENV LD_LIBRARY_PATH /usr/local/caffe2_build/lib:${LD_LIBRARY_PATH}

# Clone the Detectron repository
RUN git clone https://gitlab.com/khiemledev/detectron-vlp.git /detectron-vlp

RUN pip install --upgrade pip
RUN pip install -r /detectron-vlp/requirements.txt

# Install pycocotools
RUN git clone https://github.com/cocodataset/cocoapi.git /cocoapi && cd /cocoapi/PythonAPI && make install

WORKDIR /detectron-vlp

ENV PYTHONPATH /detectron-vlp/lib:${PYTHONPATH}
ARG PYTHONPATH /detectron-vlp/lib:${PYTHONPATH}

# Download detectron weights and config
RUN bash ./download_weights.sh
